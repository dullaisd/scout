import React from "react";

import "./style.scss";

const RedButton = props => {
  return <div {...props} className="red-button"></div>;
};

export default RedButton;
