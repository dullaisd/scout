import React from "react";
import { shallow } from "enzyme";
import RedButton from "./red-button";

describe("RedButton", () => {
  it("renders without crashing", () => {
    shallow(<RedButton />);
  });

  it("passes onClick", () => {
    const fn = jest.fn(),
      wrapper = shallow(<RedButton onClick={fn} />);

    wrapper.find(".red-button").simulate("click");

    expect(fn).toHaveBeenCalled();
  });
});
