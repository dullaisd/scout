import React from "react";
import { act } from "react-dom/test-utils"; // ES6
import { shallow, mount } from "enzyme";

describe("App", () => {
  it("renders without crashing", () => {
    jest.isolateModules(() => {
      jest.doMock("react-redux", () => ({
        useSelector: fn =>
          fn({
            date: null
          }),
        useDispatch: () => {}
      }));

      const { default: App } = require("./app");

      shallow(<App />);
    });
  });

  it("passes rates to store on successful data fetch", done => {
    jest.isolateModules(() => {
      const response = { MINE: 999 },
        jsonPromise = Promise.resolve(response),
        fetchPromise = Promise.resolve({
          status: 200,
          json: () => jsonPromise
        }),
        mockDispatch = jest.fn();
      jest.spyOn(global, "fetch").mockImplementation(() => fetchPromise);

      jest.doMock("react-redux", () => ({
        useSelector: fn =>
          fn({
            date: null
          }),
        useDispatch: () => mockDispatch
      }));

      const { default: App } = require("./app");

      const wrapper = mount(<App />);

      wrapper.find(".red-button").simulate("click");

      process.nextTick(() => {
        expect(mockDispatch).toHaveBeenCalledWith({
          payload: { MINE: 999 },
          type: "CHANGE_RATES"
        });

        global.fetch.mockClear();
        done();
      });
    });
  });

  it("shows error on failed data fetch", async done => {
    jest.isolateModules(async () => {
      const response = {},
        jsonPromise = Promise.resolve(response),
        fetchPromise = Promise.resolve({
          status: 500,
          json: () => jsonPromise
        });
      jest.spyOn(global, "fetch").mockImplementation(() => fetchPromise);

      jest.doMock("react-redux", () => ({
        useSelector: fn =>
          fn({
            date: null
          }),
        useDispatch: () => () => {}
      }));

      const { default: App } = require("./app");
      const wrapper = mount(<App />);

      await act(async () => {
        wrapper.find(".red-button").simulate("click");
      });

      process.nextTick(() => {
        wrapper.update();

        expect(wrapper.find(".modal .error-msg").text()).toBe(
          "Cat pushed our server off table, we are fixing it now!"
        );

        global.fetch.mockClear();
        done();
      });
    });
  });
});
