import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { setLoader, changeRates } from "../../store/actionCreators";
import { getExchangeRates } from "../../utils/api-calls";

import Modal from "../modal/modal";
import RedButton from "../red-button/red-button";
import Loader from "../loader/loader";
import RatesCalculations from "../rates-calculations/rates-calculations";

import "./style.scss";

const App = () => {
  const dispatch = useDispatch();
  const [error, setError] = useState("");
  const dataAvailable = useSelector(state => typeof state.date === "string");

  const loading = setting => {
    dispatch(setLoader(setting));
  };

  const handleDataFetch = async () => {
    loading(true);

    const response = await getExchangeRates();

    if (response.error.length === 0 && response.status === 200) {
      dispatch(changeRates(response.data));
    } else {
      setError("Cat pushed our server off table, we are fixing it now!");
    }

    loading(false);
  };

  return (
    <div className="App">
      {dataAvailable && <RatesCalculations></RatesCalculations>}
      {!dataAvailable && <RedButton onClick={handleDataFetch}></RedButton>}
      <Loader></Loader>
      {error && (
        <Modal
          title="Error"
          onClose={() => {
            setError("");
          }}
        >
          <div className="error-msg">{error}</div>
        </Modal>
      )}
    </div>
  );
};

export default App;
