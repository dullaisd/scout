import React from "react";
import { shallow } from "enzyme";
import RatesCalculations from "./rates-calculations";

jest.mock("react-redux", () => ({
  useSelector: fn =>
    fn({
      rates: { MINE: 777, YOURS: 0.2 }
    })
}));

describe("RatesCalculations", () => {
  it("renders without crashing", () => {
    shallow(<RatesCalculations />);
  });

  it("renders correct amount of rates", () => {
    const wrapper = shallow(<RatesCalculations />);

    expect(wrapper.find(".entry").length).toBe(2);
  });

  it("renders correct rates", () => {
    const wrapper = shallow(<RatesCalculations />);

    wrapper.find("input").simulate("change", { target: { value: "3.23" } });

    expect(
      wrapper
        .find(".entry")
        .first()
        .find(".key")
        .text()
    ).toBe("MINE");

    expect(
      wrapper
        .find(".entry")
        .first()
        .find(".value")
        .text()
    ).toBe("2509.71");
  });
});
