import React, { useState } from "react";
import { useSelector } from "react-redux";

import "./style.scss";

const RatesCalculations = () => {
  const rates = useSelector(state => state.rates);
  const [euros, setEuros] = useState(1);

  const hangleChange = e => {
    let value = parseFloat(e.target.value, 10);

    if (Number.isNaN(value) || value < 0) {
      value = "";
    } else if (value > 1000000) {
      value = 1000000;
    }

    setEuros(value);
  };

  return (
    <section className="rates">
      <input
        type="number"
        className="input"
        value={euros}
        onChange={hangleChange}
      ></input>
      <div className="entries">
        {Object.entries(rates).map(([key, value]) => {
          return (
            <div className="entry" key={key}>
              <div className="key">{key}</div>
              <div className="value">
                {parseFloat(value * euros).toFixed(2)}
              </div>
            </div>
          );
        })}
      </div>{" "}
    </section>
  );
};

export default RatesCalculations;
