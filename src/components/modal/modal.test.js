import React from "react";
import { shallow, mount } from "enzyme";

import Modal from "./modal";

describe("Modal", () => {
  it("renders without crashing", () => {
    shallow(<Modal>test</Modal>);
  });

  it("renders correct children without crashing", () => {
    const wrapper = shallow(
      <Modal>
        <div className="test-class"></div>
      </Modal>
    );

    expect(wrapper.exists(".test-class")).toBe(true);
  });

  it("renders with given classname", () => {
    const wrapper = shallow(<Modal className="correct-classname">test</Modal>);

    expect(wrapper.find("section").hasClass("correct-classname")).toEqual(true);
  });

  it("renders with correct title", () => {
    const wrapper = shallow(<Modal title="Correct title">test</Modal>);

    expect(wrapper.find(".title").text()).toEqual("Correct title");
  });

  it("handles closing", () => {
    const fn = jest.fn();
    const wrapper = mount(<Modal onClose={fn}>test</Modal>);

    wrapper.find(".close-x").simulate("click");

    expect(fn).toHaveBeenCalled();
  });

  it("passes correct width to css", () => {
    const wrapper = shallow(<Modal width={387}>test</Modal>);

    expect(wrapper.find(".modal-content").prop("style")).toEqual({
      "--width": 387
    });
  });
});
