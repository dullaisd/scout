import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import CloseX from "../close-x/close-x";

import "./style.scss";

const Modal = ({ children, className, title, onClose, width }) => {
  return (
    <section
      className={classnames({
        [className]: typeof className === "string",
        modal: true
      })}
    >
      <div className="modal-content" style={{ "--width": width }}>
        {typeof title === "string" && <div className="title">{title}</div>}
        <CloseX
          className="button-close"
          onClick={onClose}
          color="#ababab"
          thickness={1}
          size={16}
        ></CloseX>

        {children}
      </div>
    </section>
  );
};

Modal.propTypes = {
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
  title: PropTypes.string,
  onClose: PropTypes.func,
  width: PropTypes.number
};

Modal.defaultProps = {
  onClose: () => {},
  width: 600
};

export default Modal;
