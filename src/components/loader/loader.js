import React from "react";
import { useSelector } from "react-redux";

import "./style.scss";

const Loader = () => {
  const loading = useSelector(state => state.loading);

  if (!loading) {
    return null;
  }

  return (
    <div className="loading">
      <div className="spinner"></div>
    </div>
  );
};

export default Loader;
