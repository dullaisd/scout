import React from "react";
import { shallow } from "enzyme";

describe("Loader", () => {
  beforeEach(() => {
    jest.resetModules();
  });

  it("renders without crashing", () => {
    jest.doMock("react-redux", () => ({
      useSelector: () => ({})
    }));

    const { default: Loader } = require("./loader.js");

    shallow(<Loader />);
  });

  it("is not visible if state dictates it", () => {
    jest.doMock("react-redux", () => ({
      useSelector: fn =>
        fn({
          loading: false
        })
    }));

    const { default: Loader } = require("./loader.js");

    const wrapper = shallow(<Loader />);

    expect(wrapper.get(0)).toBeFalsy();
  });

  it("is visible if state dictates it", () => {
    jest.doMock("react-redux", () => ({
      useSelector: fn =>
        fn({
          loading: true
        })
    }));

    const { default: Loader } = require("./loader.js");

    const wrapper = shallow(<Loader />);

    expect(wrapper.exists(".loading")).toEqual(true);
  });
});
