import React from "react";
import { shallow } from "enzyme";

import CloseX from "./close-x";

describe("CloseX", () => {
  it("renders without crashing", () => {
    shallow(<CloseX />);
  });

  it("passed props", () => {
    const fn = jest.fn(),
      wrapper = shallow(<CloseX onClick={fn} />);

    wrapper.find(".close-x").simulate("click");

    expect(fn).toHaveBeenCalledTimes(1);
  });
});
