import React from "react";
import PropTypes from "prop-types";
import classnames from "classnames";

import "./style.scss";

const CloseX = ({ className, size, color, thickness, ...rest }) => {
  return (
    <div
      className={classnames({
        "close-x": true,
        [className]: typeof className === "string"
      })}
      {...rest}
    >
      <div
        style={{
          color,
          width: `${size}px`,
          height: `${size}px`,
          "--thickness": `${thickness}px`
        }}
        className="close"
      />
    </div>
  );
};

CloseX.propTypes = {
  size: PropTypes.number,
  color: PropTypes.string,
  className: PropTypes.string,
  thickness: PropTypes.number
};

CloseX.defaultProps = {
  size: 20,
  color: "#000000",
  thickness: 3
};

export default CloseX;
