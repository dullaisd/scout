const api = "https://api.exchangeratesapi.io";

// Simple and easy wrapper for future improvements
const request = async ({ url, method = "GET", data }) => {
  try {
    const response = await fetch(api + url, {
        method,
        ...(typeof data !== "undefined" ? { data } : {})
      }),
      json = await response.json();

    return { error: [], status: response.status, data: json };
  } catch (err) {
    return {
      error: ["fetch_failed"],
      status: err.response.data.status
    };
  }
};

export const getExchangeRates = () => {
  return request({ url: "/latest" });
};
