import types from "./types";

export const changeRates = payload => {
  return {
    type: types.CHANGE_RATES,
    payload
  };
};

export const setLoader = payload => {
  return {
    type: types.SET_LOADER,
    payload
  };
};
