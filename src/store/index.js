import { createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';

// No need to use "combineReducers", there is not enough action for that
import initialState from './initialState';
import reducer from './reducer';

export default function configureStore() {
	return createStore(reducer, initialState, composeWithDevTools());
}
