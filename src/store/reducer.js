import initialState from "./initialState";
import types from "./types";

export default (state = initialState, { type, payload = {} }) => {
  switch (type) {
    case types.CHANGE_RATES:
      return {
        ...state,
        // We could spread all of payload in, but that would spread additinal data as well, if such data would be provided
        base: payload.base,
        date: payload.date,
        rates: payload.rates
      };
    case types.SET_LOADER:
      return {
        ...state,
        // Lets make sure to always save boolean
        loading: !!payload
      };
    default:
      return state;
  }
};
